fun main() {
    val bool1 = true
    val bool2 = false

    println(bool1 && bool2)
    println(bool2 && bool2)
    println(bool2 || bool2)
}