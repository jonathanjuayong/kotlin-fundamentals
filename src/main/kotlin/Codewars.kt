class Codewars {
    fun countPositivesSumNegatives(input: Array<Int>?): Array<Int> =
        if (input == null || input.isEmpty())
            arrayOf()
        else
            input.fold(arrayOf(0, 0)) { acc, i ->
                if (i > 0)
                    arrayOf(acc[0] + 1, acc[1])
                else
                    arrayOf(acc[0], acc[1] + i)
            }

    fun isLeapYear(year: Int): Boolean =
        when {
            year % 400 == 0 -> true
            year % 100 == 0 -> false
            year % 4 == 0 -> true
            else -> false
        }

    fun alphabetWar(fight: String): String {
        val leftMap = mapOf(
            'w' to 4,
            'p' to 3,
            'b' to 2,
            's' to 1
        )

        val rightMap = mapOf(
            'm' to 4,
            'q' to 3,
            'd' to 2,
            'z' to 1
        )

        val (leftSide, rightSide) = fight.lowercase().fold(Pair<Int, Int>(0, 0)) { acc, i ->
            val (left, right) = acc
            when (i) {
                in "wbps" -> Pair(left + leftMap[i]!!, right)
                in "mqdz" -> Pair(left, right + rightMap[i]!!)
                else -> acc
            }
        }
        return when {
            leftSide > rightSide -> "Left side wins!"
            leftSide < rightSide -> "Right side wins!"
            else -> "Let's fight again!"
        }
    }
}