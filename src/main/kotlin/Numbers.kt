fun main() {
    val bytes = Byte.MAX_VALUE
    val short = Short.MAX_VALUE
    val integer = Int.MAX_VALUE
    val number2 = 9000000000000000000
    printType(bytes)
    printType(short)
    printType(integer)
    printType(number2)
    println(8.0 / 3)
}

fun printType(any: Any) {
    println(any::class.simpleName)
}